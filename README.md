# GNU Toolchain for the A-profile architecture

The GNU Toolchain for the Cortex-A Family are integrated and validated packages featuring the GCC compiler, libraries and other GNU tools necessary for software development on devices based on the Arm Cortex-A processors or the Arm A-profile architecture.


These toolchains are based on Free Software Foundation's (FSF) GNU open source tools.


Info: https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-a

Downloads: https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-a/downloads

